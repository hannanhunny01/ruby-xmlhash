#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: xmlhash 1.3.9 ruby lib
# stub: ext/xmlhash/extconf.rb

Gem::Specification.new do |s|
  s.name = "xmlhash".freeze
  s.version = "1.3.9"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "homepage_uri" => "https://github.com/coolo/xmlhash" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Stephan Kulow".freeze]
  s.date = "2023-01-20"
  s.description = "A small C module that wraps libxml2's xmlreader to parse a XML\nstring into a ruby hash".freeze
  s.email = ["coolo@suse.com".freeze]
  s.extensions = ["ext/xmlhash/extconf.rb".freeze]
  s.extra_rdoc_files = ["History.txt".freeze, "Manifest.txt".freeze, "README.txt".freeze]
  s.files = [".autotest".freeze, ".travis.yml".freeze, "Gemfile".freeze, "History.txt".freeze, "Manifest.txt".freeze, "README.txt".freeze, "Rakefile".freeze, "ext/xmlhash/extconf.rb".freeze, "ext/xmlhash/xmlhash.c".freeze, "lib/xmlhash.rb".freeze, "test/test_xmlhash.rb".freeze]
  s.homepage = "https://github.com/coolo/xmlhash".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--main".freeze, "README.txt".freeze]
  s.rubygems_version = "3.2.5".freeze
  s.summary = "A small C module that wraps libxml2's xmlreader to parse a XML string into a ruby hash".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<hoe>.freeze, ["~> 3.26"])
    s.add_runtime_dependency(%q<pkg-config>.freeze, [">= 0"])
    s.add_development_dependency(%q<rake-compiler>.freeze, [">= 0"])
    s.add_development_dependency(%q<rdoc>.freeze, [">= 4.0", "< 7"])
  else
    s.add_dependency(%q<hoe>.freeze, ["~> 3.26"])
    s.add_dependency(%q<pkg-config>.freeze, [">= 0"])
    s.add_dependency(%q<rake-compiler>.freeze, [">= 0"])
    s.add_dependency(%q<rdoc>.freeze, [">= 4.0", "< 7"])
  end
end
